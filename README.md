## 项目安装/启动
1. npm install
2. npm start

## 技术栈说明
1. TensorFlow: 机器学习技术支持 - 屏蔽非法图片(黄赌毒)
2. webRTC: 语音通话技术支持
3. express: 服务端技术支持
4. socket: 图文聊天技术支持
5. React: 页面技术支持
## 技术栈补充

此处将补充各种技术栈的细节.Peace
