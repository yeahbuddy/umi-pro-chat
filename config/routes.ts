﻿import Authorized from '@/utils/authority';
export default [
  {
    path: '/',
    component: '../layouts/BlankLayout',
    // 鉴权 有坑 鉴权下级一定要component
    authority: ['admin', 'user'],
    routes: [
      {
        path: '/user',
        component: '../layouts/UserLayout',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './User/login',
          },
          {
            name: 'register',
            path: '/user/register',
            component: './User/register',
          },
        ],
      },
      {
        path: '/',
        component: '../layouts/SecurityLayout',
        routes: [
          {
            path: '/',
            component: '../layouts/BasicLayout',
            authority: ['admin', 'user'],
            routes: [
              {
                path: '/',
                redirect: '/welcome',
              },
              {
                path: '/welcome',
                name: 'welcome',
                icon: 'smile',
                component: './Welcome',
              },
              {
                path: '/proChat',
                name: 'proChat',
                icon: 'WechatOutlined',
                component: './ProChat/ProChat',
              },
              {
                path: '/data',
                name: 'data',
                icon: 'LineChartOutlined',
                component: './Data/Data',
              },
              {
                path: '/todo',
                name: 'Todo',
                icon: 'HighlightOutlined',
                component: './todo/todo',
              },
              {
                path: '/admin',
                name: 'admin',
                icon: 'crown',
                component: './Admin',
                authority: ['admin'],
                routes: [
                  {
                    path: '/admin/sub-page',
                    name: 'sub-page',
                    icon: 'smile',
                    component: './Welcome',
                    authority: ['admin'],
                  },
                ],
              },
              {
                name: 'list.table-list',
                icon: 'table',
                path: '/list',
                component: './TableList',
              },
              {
                component: './404',
              },
            ],
          },
          {
            component: './404',
          },
        ],
      },
    ],
  },
  {
    component: './404',
  },
];
