import { axiosInstance } from './config';

export const test = () => {
  return axiosInstance.get('/');
};

export const login = (param: any) => {
  return axiosInstance.post('/login', param);
};
