import axios from 'axios';
import { message } from 'antd';

export const baseURL = 'http://localhost:3000/api';
// axios 的实例及拦截器配置

const axiosInstance = axios.create({
  baseURL,
});

axiosInstance.interceptors.response.use(
  (res) => res.data,
  () => {
    message.warning('网络繁忙～');
  },
);

export { axiosInstance };
