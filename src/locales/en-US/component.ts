export default {
  'component.tagSelect.expand': 'Expand',
  'component.tagSelect.collapse': 'Collapse',
  'component.tagSelect.all': 'All',
  'pro.welcome.title': 'Click on the upper right corner to switch languages',
  'pro.welcome.content': 'Please join us'
};
