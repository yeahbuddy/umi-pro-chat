export default {
  'pro.welcome.title': '点击右上角切换语言',
  'component.tagSelect.expand': '展开',
  'component.tagSelect.collapse': '收起',
  'component.tagSelect.all': '全部',
  'pro.welcome.content': '欢迎加入ProChat大家庭'
};
