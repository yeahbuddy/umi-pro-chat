import React, { useEffect } from 'react';
import { Card } from 'antd';
import * as echarts from 'echarts';
import 'echarts/lib/chart/bar';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';

const Data = () => {
  useEffect(() => {
    const container: any = document.getElementById('echart');
    console.log(container, echarts);

    // 基于准备好的dom，初始化echarts实例
    const myChart = echarts.init(container);
    // 绘制图表
    myChart.setOption({
      title: { text: 'ECharts 入门示例' },
      tooltip: {},
      xAxis: {
        data: ['衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子'],
      },
      yAxis: {},
      series: [
        {
          name: '销量',
          type: 'bar',
          data: [5, 20, 36, 10, 10, 20],
        },
      ],
    });
  }, []);

  return (
    <div
      style={{
        margin: -24,
      }}
    >
      <Card
        type="inner"
        style={{
          width: '100%',
          height: '100%',
          boxShadow: 'none',
        }}
        bodyStyle={{
          padding: 0,
          height: 'calc(100vh - 50px)',
        }}
      >
        <div id="echart" style={{ width: '100%', height: '100%' }} />
      </Card>
    </div>
  );
};
export default React.memo(Data);
