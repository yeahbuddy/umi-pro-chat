import React from 'react';
import {Row, Col} from 'antd';
import ProContacts from "@/pages/ProChat/ProContacts/ProContacts";
import ProMain from '@/pages/ProChat/ProMain/ProMain';
import PublicRightClick from "@/pages/ProChat/components/PublicRightClick";

const ProChat = () => {
  // 屏蔽右键事件
  // const handleContextMenu = (event: any) => {
  //   event.preventDefault();
  // };
  // useEffect(() => {
  //   document.addEventListener('contextmenu', handleContextMenu);
  //   return () => {
  //     document.removeEventListener('contextmenu', handleContextMenu);
  //   }
  // }, []);
  // 渲染
  return (
    <div style={{ margin: -24 }}>
      <Row>
        <Col span={5}>
          <ProContacts />
        </Col>
        <Col span={19}>
          <ProMain />
        </Col>
      </Row>
      <PublicRightClick />
    </div>
  )
};

export default React.memo(ProChat);
