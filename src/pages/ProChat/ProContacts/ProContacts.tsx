// 联系人
import React, { useState } from 'react';
import { Card, List, Row, Col } from 'antd';
import { PlusOutlined, SearchOutlined } from '@ant-design/icons';
import ProAvatar from './components/ProAvatar';
import '@/pages/ProChat/ProChat.less';
import './ProContacts.less';

const mockContact: any = [];

for (let i = 0; i < 5; i += 1) {
  if (i === 1) {
    mockContact.push({
      id: i,
      name: `Pro King ${i}`,
      howTimeAgo: '',
      tips: `[16 Messages] Welcome to our message`,
      message: 5,
      mute: true,
      status: 'muted',
      isGroup: true,
    });
  }  else {
    mockContact.push({
      id: i,
      name: `King ${i}`,
      howTimeAgo: '',
      tips: `[16 Messages] Welcome to our message`,
      message: 1,
      mute: false,
      status: 'onLine',
      group: false,
    });
  }
}
const ProContacts = () => {
  const [data] = useState(mockContact);
  const handleItemClick = (item: any) => {
    console.log('单击联系人', item);
  };
  // 联系人列表
  const renderProList = () => (
    <List
      split={false}
      bordered={false}
      dataSource={data}
      renderItem={(item: any, index: number) => {
        return (
          <div onClick={() => handleItemClick(item)} className="WeChatContactsItemBox" key={index}>
            <Row className="WeChatContactsItem">
              <Col span={21}>
                <ProAvatar item={item} />
              </Col>
              <Col span={3}>
                <div className="howTimeAgoAndTipsIcon">
                  <div className="howTimeAgo">just</div>
                </div>
              </Col>
            </Row>
          </div>
        );
      }}
    />
  );
  // 渲染
  return (
    <Card
      type="inner"
      style={{
        width: '100%',
        boxShadow: 'none',
      }}
      className="proContactsCard"
      bodyStyle={{
        padding: 0,
        height: 'calc(100vh - 50px)'
      }}
    >
      <div className="WeChatContactsHeader">
        <div className="title">findChat</div>
        <div className="iconBox">
          <SearchOutlined />
          <PlusOutlined className="add" />
        </div>
      </div>
      {renderProList()}
    </Card>
  );
};
export default React.memo(ProContacts);
