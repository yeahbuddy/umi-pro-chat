// 联系人头像组件
import React from 'react';
import PublicAvatar from "@/pages/ProChat/components/PublicAvatar";


const ProAvatar = (props: any) => {
  const { isGroup, name, message, status } = props.item;
    // 单聊
    const renderSingerChat = () => {
        return (
            <div className="ProAvatar">
                <div className="avatarBox">
                    <div>
                      <PublicAvatar isGroup={isGroup} status={status} />
                    </div>
                    <div className="nameAndTipsWordBox">
                        <div className="name">{ name }</div>
                        <div className="tipsWord">
                            [{message} Messages] Welcome to our m
                        </div>
                    </div>
                </div>
            </div>
        )
    };
    // 群聊
    const renderGroupChat = () => {
        return(
            <div className="ProAvatar">
                <div className="avatarBox">
                    <div>
                        <PublicAvatar isGroup={isGroup} status={status} />
                    </div>
                    <div className="nameAndTipsWordBox">
                      <div className="name">{ name }</div>
                      <div className="tipsWord">
                        [{message} Messages] Welcome to our m
                      </div>
                    </div>
                </div>
            </div>
        )
    };
    // 渲染联系人
    const renderChat = () => (
      <div className="ProAvatar">
        <div className="avatarBox">
          <div>
            <PublicAvatar isGroup={isGroup} status={status} />
          </div>
          <div className="nameAndTipsWordBox">
            <div className="name">{ name }</div>
            <div className="tipsWord">
              [{message} Messages] Welcome to our m
            </div>
          </div>
        </div>
      </div>
    );
    // 渲染
    return renderChat();
};

export default React.memo(ProAvatar);
