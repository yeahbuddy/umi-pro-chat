// 聊天内容
import React, { useState } from 'react';
import '@/pages/ProChat/ProChat.less';
import { Drawer, Button, notification } from 'antd';
import {
  UserOutlined,
  SmileOutlined
} from '@ant-design/icons';
import ProHeaderAvatar from './components/ProHeaderAvatar';
import ProMainContent from './components/ProMainContent';
import ProMainFooter from './components/ProMainFooter';
import ProDrawerContent from "@/pages/ProChat/ProMain/components/ProDrawerContent";

const ProMain = () => {
  const [ isDrawerShow, setIsDrawerShow ] = useState<boolean>(false);
  // 打开抽屉
  const handleDrawerShow = () => {
    notification.open({
      message: 'Notification',
      duration: 1,
      description:
        '按ESC可关闭抽屉',
      icon: <SmileOutlined style={{ color: '#108ee9' }} />,
    });
    setIsDrawerShow(true);
  };
  // 总渲染
  return (
    <div className='weChatMainContainer'>
      {/* 聊天内容 */}
      <div className="weChatMainWrap">
        <div className="weChatMainHeader">
          <ProHeaderAvatar/>
          <div>
            <Button
              className='tool'
              type="primary"
              shape="circle"
              icon={<UserOutlined/>}
              style={{
                width: 38,
                height: 38
              }}
            />
            <Button
              className='tool'
              type="primary"
              shape="circle"
              icon={<UserOutlined/>}
              style={{
                width: 38,
                height: 38
              }}
            />
            <Button
              className='tool'
              type="primary"
              shape="circle"
              icon={<UserOutlined/>}
              style={{
                width: 38,
                height: 38
              }}
              onClick={() => handleDrawerShow()}
            />
          </div>
        </div>
        <ProMainContent/>
        <ProMainFooter/>
      </div>
      {/* 群聊抽屉 */}
      <Drawer
        title="Group Chat"
        mask={false}
        onClose={() => setIsDrawerShow(false)}
        visible={isDrawerShow}
        getContainer={false}
        style={{position: 'absolute'}}
        headerStyle={{ height: 74}}
        keyboard={true}
        bodyStyle={{
          padding: 0,
          fontSize: 12,
          height: '100%',
        }}
      >
        <ProDrawerContent />
      </Drawer>
    </div>
  )
};

export default React.memo(ProMain)
