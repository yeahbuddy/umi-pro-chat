// 抽屉内容
import React from 'react';
import { Avatar, Badge, Button, Input, Popover, Switch } from 'antd';
import {
  CheckOutlined,
  UnorderedListOutlined,
  UserAddOutlined,
  UserDeleteOutlined,
  UserOutlined
} from '@ant-design/icons';
import '@/pages/ProChat/ProMain/styles/ProDrawerContent.less';

const { Search } = Input;

const ProDrawerContent = () => {
  const renderStatus = (status: any) => {
    switch (status) {
      case 0:
        return <CheckOutlined style={{
          color: "#fff",
          border: "2px solid #fff",
          borderRadius: "50%",
          backgroundColor: "red"
        }}/>;
      case 1:
        return <CheckOutlined style={{
          color: "#fff",
          border: "2px solid #fff",
          borderRadius: "50%",
          backgroundColor: "#64C8BC"
        }}/>;
      default:
        return null;
    }
  };
  const renderContent = (
    <div className="ProHeaderAvatarPopover">
      <div className="ProHeaderAvatarPopoverInfo">
        <div className="PopoverInfoImageBox">
          <div>
            <div className="PopoverInfoName">Fanny Jensen</div>
            <div className="PopoverInfoNumb">1008</div>
            <div className="PopoverInfoTools">
              <Button type="primary" shape="circle" style={{ transform: 'scale(0.75)' }}>
                A
              </Button>
              <Button type="primary" shape="circle" style={{ transform: 'scale(0.75)' }}>
                A
              </Button>
              <Button type="primary" shape="circle" style={{ transform: 'scale(0.75)' }}>
                A
              </Button>
              <Button type="primary" shape="circle" style={{ transform: 'scale(0.75)' }}>
                A
              </Button>
            </div>
          </div>
          <div className="PopoverInfoAvatar">
            <Badge
              style={{ backgroundColor: 'red' }}
              size="small"
              count={renderStatus(0)}
              offset={[-6, 48]}>
              <Avatar size={55} icon={<UserOutlined />} />
            </Badge>
          </div>
        </div>
        <div className="status">Im currently out of the office</div>
      </div>
      <div className="ContactWay">
        <div className="ContactWayItem">
          <div>
            <UnorderedListOutlined />
          </div>
          <div>eve@English.com</div>
        </div>
        <div className="ContactWayItem">
          <div>
            <UnorderedListOutlined />
          </div>
          <div>eve@English.com</div>
        </div>
      </div>
    </div>
  );
  const renderItemMember = () => {
    return [0,1,2,3,4,5].map(item => {
      return (
        <Popover key={item} placement="leftTop" content={renderContent} trigger="click">
          <div className="avatarBox">
            <div className="avatarBoxLeft">
              <div>
                <div>
                  <Badge
                    style={{ backgroundColor: 'red' }}
                    size="small"
                    count={renderStatus(1)}
                    offset={[-3, 30]}
                    title="onLine">
                    <Avatar size={36} icon={<UserOutlined />} />
                  </Badge>
                </div>
              </div>
              <div className="name">Rosie Paul</div>
            </div>
            { item === 0 && <div className="owner">owner</div> }
          </div>
        </Popover>
      );
    });
  };
  const renderDrawerContent = () => {
    return (
      <div className="chatDrawerContent">
        <div className="groupName">Members (8)</div>
        <div className="searchBox">
          <Search className="search" placeholder="search" onSearch={value => console.log(value)} />
        </div>
        <div className="avatarBoxWrap">
          <div className="addBox">
            <div>
              <Button type="primary" shape="circle" icon={<UserAddOutlined />} style={{ height: 36, width: 36 }} />
            </div>
            <div className="name bold">Add Members</div>
          </div>
          {renderItemMember()}
        </div>
        <div className="groupName">Group Settings</div>
        <div className="notifications">
          <div className="name">Notifications</div>
          <div className="enb_switch">
            <Switch />
          </div>
        </div>
        <Button icon={<UserDeleteOutlined />} className="fixBottom">
          Leave Group
        </Button>
      </div>
    );
  };
  return renderDrawerContent();
};

export default React.memo(ProDrawerContent);
