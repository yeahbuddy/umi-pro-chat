// 聊天内容头像组件
import React from 'react';
import {Avatar, Badge, Popover,Button, Space} from "antd";
import {
    UserOutlined,
    CheckOutlined,
    MailOutlined
} from '@ant-design/icons';
import '@/pages/ProChat/ProChat.less';
import PublicAvatar from "@/pages/ProChat/components/PublicAvatar";

const ProHeaderAvatar = () => {
    const renderStatus = (status: number) => {
        switch (status) {
            case 0:
                return <CheckOutlined style={{
                    color: "#fff",
                    border: "2px solid #fff",
                    borderRadius: "50%",
                    backgroundColor: "red"
                }}/>;
            case 1:
                return <CheckOutlined style={{
                    color: "#fff",
                    border: "2px solid #fff",
                    borderRadius: "50%",
                    backgroundColor: "#64C8BC"
                }}/>;
            default:
                return  null;
        }
    };
    // 头像内容
    const renderContent = (
        <div className="ProHeaderAvatarPopover">
            <div className="ProHeaderAvatarPopoverInfo">
                <div className="PopoverInfoImageBox">
                    <div>
                        <div className="PopoverInfoName">Fanny Jensen</div>
                        <div className="PopoverInfoNumb">1008</div>
                        <div className="PopoverInfoTools">
                            <Space>
                                <Button type="primary" shape="circle" size="small">
                                    A
                                </Button>
                                <Button type="primary" shape="circle" size="small">
                                    A
                                </Button>
                                <Button type="primary" shape="circle" size="small">
                                    A
                                </Button>
                                <Button type="primary" shape="circle" size="small">
                                    A
                                </Button>
                            </Space>
                        </div>
                    </div>
                    <div className="PopoverInfoAvatar">
                        <Badge
                            style={{
                                backgroundColor: "red"
                            }}
                            size="small"
                            count={
                                renderStatus(1)
                            }
                            offset={[-6, 48]}
                            title="onLine"
                        >
                            <Avatar
                                size={55}
                                icon={<UserOutlined/>}
                            />
                        </Badge>
                    </div>
                </div>
                <div className="status">I'm currently out of the office</div>
            </div>
            <div className="ContactWay">
                <div className="ContactWayItem">
                    <div><MailOutlined /></div>
                    <div>eve@English.com</div>
                </div>
                <div className="ContactWayItem">
                    <div><MailOutlined /></div>
                    <div>eve@English.com</div>
                </div>
            </div>
        </div>
    );
    return (
        <div className="ProHeaderAvatar">
            <div className="avatarBox">
                <Popover
                  overlayClassName="ProHeaderAvatarPopTop"
                  placement="bottomLeft"
                  content={renderContent}
                  trigger="click"
                >
                    <div>
                      <PublicAvatar status={'onLine'} isGroup={false} />
                    </div>
                </Popover>
                <div className="nameAndTipsWordBox">
                    <div className="name">King</div>
                    <div className="tipsWord">
                        [Available] onLine
                    </div>
                </div>
            </div>
        </div>
    )
};

export default React.memo(ProHeaderAvatar);
