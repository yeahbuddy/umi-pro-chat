// 聊天内容
import React from 'react';
import '@/pages/ProChat/ProChat.less';
import {Avatar,Image } from 'antd';
import {
  UserOutlined,
  FileTextOutlined,
  EllipsisOutlined
} from '@ant-design/icons';

const ProMainContent = () => {
  // 他人聊天气泡
  const renderOtherMessage = () => {
    return (
      <>
        <div className='messageOther messageContent'>
          <div className="messageContentItem">
            <div className="messageContentAvatar">
              <Avatar size={44} icon={<UserOutlined/>}/>
            </div>
            <div className='messageContentTxt messageContentTxtMT2'>
              say something say something
              say something say something
            </div>
          </div>
        </div>
        <div className='messageOther messageContent'>
          <div className="messageContentItem">
            <div className="messageContentAvatar">

            </div>
            <div className='messageContentFile'>
              <div className="messageContentIcon">
                <FileTextOutlined/>
              </div>
              <div className="messageContentDetail">
                <div className='messageContentFileNameBox'>
                  <div className="fileName">
                    English.txt
                  </div>
                  <div className='fileSize'>
                    123kb
                  </div>
                </div>
                <div>
                  <EllipsisOutlined/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='messageOther messageContent'>
          <div className="messageContentItem messageContentTxtMT6">
            <div className="messageContentAvatar">
            </div>
            <div className='messageContentImg'>
              <Image
                width={279}
                src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
              />
            </div>
          </div>
        </div>
      </>
    )
  };
  // 自己聊天气泡
  const renderMyselfMessage = () => {
    return(
      <>
        <div className='messageMyself'>
          <div className="messageContentItem">
            <div className='messageContentTxt messageContentTxtMT2'>
              say something
            </div>
            <div className="messageContentAvatar">
              <Avatar size={44} icon={<UserOutlined />} />
            </div>
          </div>
        </div>
        <div className='messageMyself'>
          <div className="messageContentItem">
            <div className='messageContentFile'>
              <div className="messageContentIcon">
                <FileTextOutlined />
              </div>
              <div className="messageContentDetail">
                <div className='messageContentFileNameBox'>
                  <div className="fileName">
                    English.txt
                  </div>
                  <div className='fileSize'>
                    123kb
                  </div>
                </div>
                <div>
                  <EllipsisOutlined />
                </div>
              </div>
            </div>
            <div className="messageContentAvatar">
            </div>
          </div>
        </div>
        <div className='messageMyself' >
          <div className="messageContentItem">
            <div className="messageContentImg messageContentTxtMT6">
              <Image
                width={279}
                src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
              />
            </div>
            <div className="messageContentAvatar">

            </div>
          </div>
        </div>
      </>
    )
  };
  return (
    <div
      className="weChatMainContent"
      id="weChatMainContent"
    >
      <div className='weChatMainContentItem'>
        <div className='messageTime'>
          Today 19:22
        </div>
        {renderOtherMessage()}
        {renderMyselfMessage()}
      </div>
    </div>
  )
}

export default React.memo(ProMainContent)
