import React, {useState} from 'react';
import '@/pages/ProChat/ProChat.less';
import {Input, Upload, Mentions, Avatar, Popover, Space} from 'antd';
import {
  FolderOutlined,
  SmileOutlined,
  GatewayOutlined,
  SendOutlined,
  PhoneOutlined,
  VideoCameraAddOutlined
} from '@ant-design/icons';
// 引入表情
import emojiList from '@/pages/ProChat/utils/emoji.json';
// 引入样式
import '@/pages/ProChat/ProMain/styles/ProMainFooter.less';
// ant组件
const {TextArea, Search} = Input;
const {Dragger} = Upload;

const ProMainFooter = () => {
  // useState
  const [state, setState] = useState<any>({ isEmojiShow: false, emojiIndex: null });
  const handleSetState = () => {
    setState({
      ...state,
      isEmojiShow: !state.isEmojiShow
    })
  };
  // 播放表情gif
  const handleSwitchEmoji = (emojiIndex: number, leave: boolean = false) => {
    // 鼠标移开表情
    if(leave){
      setState({
        ...state,
        emojiIndex: null,
      })
    }else {
      setState({
        ...state,
        emojiIndex,
      })
    }
  };
  // 渲染表情item
  const renderEmojiItem = () => {
    return emojiList.map((item: any, index: number) => {
      const emojiImg =
        state.emojiIndex === index ? (
          <img src={require(`../../emoji/${item.hover}`)} alt="表情包" style={{ width: 25, height: 25 }} />
        ) : (
          <img src={require(`../../emoji/${item.src}`)} alt="表情包" style={{ width: 25, height: 25 }} />
        );
      return (
        <div
          className="emojiItem"
          key={item.info}
          onMouseEnter={() => handleSwitchEmoji(index)}
          onMouseLeave={() => handleSwitchEmoji(index,true)}>
          {emojiImg}
        </div>
      );
    });
  };
  // 总渲染表情
  const renderEmojiBox = () => {
    return (
      <div className="emojiBox">
        <Search placeholder="input search text" onSearch={value => console.log(value)}/>
        <div className="emojiItemBox">{renderEmojiItem()}</div>
      </div>
    );
  };
  // 总渲染
  return (
    <div className='weChatMainFooter'>
      <div className='weChatMainToolsBox'>
        <div className='weChatMainToolsBoxLeft'>
          <Space size={16}>
            <Popover
              placement="topLeft"
              overlayClassName="proFooterEmojiPopTop"
              content={renderEmojiBox()}
              onVisibleChange={() => handleSetState()}
              trigger="click"
            >
              <SmileOutlined onClick={() => handleSetState()}  className={ state.isEmojiShow ? 'blue' : '' } />
            </Popover>
            <GatewayOutlined />
            <FolderOutlined />
          </Space>
        </div>
        <div className="weChatMainToolsBoxRight">
          <Space size={16}>
            <PhoneOutlined />
            <VideoCameraAddOutlined />
          </Space>

        </div>

      </div>
      {/*  */}
      <div className='weChatMainInputBox'>
        <div className='weChatMainInputWrap'>
          {
            false &&
            <Dragger>
              <p>Drop your files here</p>
            </Dragger>
          }
          {
            true && <TextArea
              autoSize={{minRows: 1, maxRows: 6}}
              maxLength={5000}
              placeholder="Message Fanny Jensen"
              bordered={false}
              style={{
                background: "#F5F6FA",
                borderRadius: 4,
                fontSize: 12,
                color: "#333",
                height: 42,
                lineHeight: 2,
                maxHeight: 176,
                resize: "none"
              }}
            />
          }
        </div>
        <div className='weChatMainInputBoxSent'>
          <SendOutlined/>
        </div>
      </div>
    </div>
  )
};

export default React.memo(ProMainFooter)
