import React from 'react';
import { Avatar, Badge, Button, Icon, Popover } from 'antd';
import PropTypes from 'prop-types';
import '@/pages/ProChat/ProChat.less';

const ChatHeader = (props: any) => {
  const { isDrawerShow, setIsDrawerShow } = props;
  // 头像状态
  const renderStatus = (status: number) => {
    switch (status) {
      case 0:
        return (
          <Icon
            type="check"
            style={{
              color: '#fff',
              border: '2px solid #fff',
              borderRadius: '50%',
              backgroundColor: '#64C8BC',
            }}
          />
        );
      case 1:
        return <Icon type="check-circle" theme="filled" />;
      default:
        return null;
    }
  };
  // 内容
  const renderContent = (
    <div className="ProHeaderAvatarPopover">
      <div className="ProHeaderAvatarPopoverInfo">
        <div className="PopoverInfoImageBox">
          <div>
            <div className="PopoverInfoName">Fanny Jensen</div>
            <div className="PopoverInfoNumb">1008</div>
            <div className="PopoverInfoTools">
              <Button type="primary" shape="circle" style={{ transform: 'scale(0.75)' }}>
                A
              </Button>
              <Button type="primary" shape="circle" style={{ transform: 'scale(0.75)' }}>
                A
              </Button>
              <Button type="primary" shape="circle" style={{ transform: 'scale(0.75)' }}>
                A
              </Button>
              <Button type="primary" shape="circle" style={{ transform: 'scale(0.75)' }}>
                A
              </Button>
            </div>
          </div>
          <div className="PopoverInfoAvatar">
            <Badge
              style={{ backgroundColor: 'red' }}
              size="small"
              count={renderStatus(1)}
              offset={[-6, 48]}>
              <Avatar size={55} src="http://p0.meituan.net/200.0/dpdeal/094d885b9ecf040f1f79ed2dd22f00da1198981.jpg" />
            </Badge>
          </div>
        </div>
        <div className="status">Im currently out of the office</div>
      </div>
      <div className="ContactWay">
        <div className="ContactWayItem">
          <div>
            <Icon type="unordered-list" />
          </div>
          <div>eve@English.com</div>
        </div>
        <div className="ContactWayItem">
          <div>
            <Icon type="unordered-list" />
          </div>
          <div>eve@English.com</div>
        </div>
      </div>
    </div>
  );
  // 单人聊天头像
  const renderSingerChat = (
    <div className="ProHeaderAvatar">
      <div className="avatarBox">
        <Popover
          overlayClassName="ProHeaderAvatarPopTop"
          placement="bottomLeft"
          content={renderContent}
          trigger="click">
          <div>
            <Badge
              style={{ backgroundColor: 'red' }}
              size="small"
              count={renderStatus(0)}
              offset={[-6, 38]}
              title="onLine">
              <Avatar size={44} icon="user" />
            </Badge>
          </div>
        </Popover>
        <div className="nameAndTipsWordBox">
          <div className="name">Fanny Jensen</div>
          <div className="tipsWord">[Available] XXXXXXX</div>
        </div>
      </div>
    </div>
  );
  // 多人聊天
  const renderGroupChat = (
    <div className="ProHeaderAvatar">
      <div className="avatarBox">
        <div>
          <Badge
            style={{ backgroundColor: 'red' }}
            size="small"
            count={renderStatus(0)}
            offset={[-6, 38]}
            title="onLine">
            <Avatar
              size={44}
              src="http://p0.meituan.net/200.0/dpdeal/094d885b9ecf040f1f79ed2dd22f00da1198981.jpg"
              onClick={() => setIsDrawerShow(!isDrawerShow)}
            />
          </Badge>
        </div>
        <div className="nameAndTipsWordBox">
          <div className="name">Fanny Jensen/renderGroupChat</div>
          <div className="tipsWord">[Available] 2人群聊</div>
        </div>
      </div>
    </div>
  );
  return (
    <div className="weChatMainHeader">
      {renderGroupChat}
      <div>
        <Button
          className="tool"
          type="primary"
          shape="circle"
          icon="video-camera"
          style={{
            width: 38,
            height: 38,
          }}
        />
        <Button
          className="tool"
          type="primary"
          shape="circle"
          icon="phone"
          style={{
            width: 38,
            height: 38,
          }}
        />
        <Button
          className="tool"
          type="primary"
          shape="circle"
          icon="plus"
          style={{
            width: 38,
            height: 38,
          }}
        />
      </div>
    </div>
  );
};
ChatHeader.propTypes = {
  setIsDrawerShow: PropTypes.func,
  isDrawerShow: PropTypes.bool,
};
export default React.memo(ChatHeader);
