// 公共头像
import React from 'react';
import {Avatar, Badge} from "antd";
import {
  UserOutlined,
  WechatFilled
} from '@ant-design/icons';
import { Colors, Titles } from '@/pages/ProChat/utils/enum';

interface IProps {
  status: string,
  isGroup: boolean
}

const PublicAvatar = (props: IProps) => {
  // 三种状态: 在线、离线、静音
  const { status, isGroup } = props;
  // const colors = {
  //   0: '#b3b7b9',
  //   1: '#87d068',
  //   2: '#f24c32'
  // };
  // const titles = {
  //   0: 'offLine',
  //   1: 'onLine',
  //   2: 'muted'
  // };
  // 总渲染
  return (
    <div className="publicAvatar">
      <Badge
        dot
        color={ Colors[status] }
        size='small'
        offset={[-6, 38]}
        status='default'
        title={ Titles[status] }
      >
        <Avatar
          size={44}
          src={!isGroup && 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'}
          icon={ isGroup ? <WechatFilled style={{ color: '#fff'}} /> : <UserOutlined />}
        />
      </Badge>
    </div>
  )
};

export default React.memo(PublicAvatar);
