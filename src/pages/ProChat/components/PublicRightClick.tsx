// 右键菜单
import React, { useEffect, useState, useRef } from 'react';

interface iStyle {
  position: any,
  left: number,
  top: number
}

const PublicRightClick = () => {
  // 显示/隐藏
  const [show, setShow] = useState<boolean>(false);
  // 改变位置
  const [style, setStyle] = useState<iStyle>({
    position: 'fixed', left: 300, top: 200
  });
  // 获得show最新值
  const showRef = useRef();
  const rightClickRef = useRef<any>();
  // 右键点击
  const handleContextMenu = (event: any) => {
    event.preventDefault();
    // 先显示才能捕捉到Ref
    setShow(true);
    // 获得点击的位置
    let { clientX, clientY } = event;
    // 文档显示区的宽度
    const screenW: number = window.innerWidth;
    const screenH: number = window.innerHeight;
    // 右键菜单的宽度
    const rightClickRefW: number = rightClickRef.current.offsetWidth;
    const rightClickRefH: number = rightClickRef.current.offsetHeight;

    // right为true，说明鼠标点击的位置到浏览器的右边界的宽度可以放contextmenu。
    // 否则，菜单放到左边。
    const right = (screenW - clientX) > rightClickRefW;
    const top = (screenH - clientY) > rightClickRefH;
    clientX = right ?  clientX + 6 : clientX - rightClickRefW - 6;
    clientY = top ? clientY + 6 : clientY - rightClickRefH - 6;

    setStyle({
      ...style,
      left: clientX,
      top: clientY
    });

  };
  // 点击事件
  const handleClick = (event: any) => {
    // 如果右键菜单不出现则不做逻辑处理
    if(!showRef.current) return;
    // 点击目标不在右键菜单里关闭
    if (event.target.parentNode !== rightClickRef.current){
      setShow(false)
    }
  };

  // 滑动关闭右键功能
  const setShowFalse = () => {
    // 如果右键菜单不出现则不做逻辑处理
    // eslint-disable-next-line no-useless-return
    if(!showRef.current) return;
    // 滚动直接关闭
    setShow(false)
  };
  // 生命周期监听
  useEffect(() => {
    document.addEventListener('contextmenu', handleContextMenu);
    document.addEventListener('click', handleClick,true);
    document.addEventListener('scroll', setShowFalse, true);
    return () => {
      document.removeEventListener('contextmenu', handleContextMenu);
      document.removeEventListener('click', handleClick,true);
      document.removeEventListener('scroll', setShowFalse, true);
    }
  }, []);
  useEffect(() => {
    // showRef.current = show;
  }, [show]);
  // 渲染右键
  const renderContentMenu = () => (
    <div ref={rightClickRef as any} className="WeChatContactsAvatarTools" style={style} >
      <div className="rightClickItems">
        Mark as unread
      </div>
      <div className="rightClickItems">
        Mute Notifications
      </div>
      <div className="rightClickItems">
        Remove
      </div>
      <div className="rightClickItems">
        Clear Chat History
      </div>
    </div>
  );
  // 总渲染
  return show ? renderContentMenu() : null;
};

export default React.memo(PublicRightClick);
