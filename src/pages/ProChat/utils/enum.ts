// 枚举集合

// 头像状态颜色集合
export enum Colors  {
  offLine = '#b3b7b9',
  onLine = '#87d068',
  muted = '#f24c32'
}
// 头像提示语
export enum Titles {
  offLine,
  onLine,
  muted,
}
