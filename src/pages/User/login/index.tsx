import React from 'react';
import { Card, Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { login } from '@/api/api';

const Login = () => {
  const onFinish = (values: any) => {
    login(values).then((res) => {
      console.log('登陆状态', res);
    });
  };

  return (
    <Card
      title="请先登陆"
      bodyStyle={{ padding: '24px 24px 0 24px' }}
      bordered={false}
      style={{ width: 400, margin: '0 auto' }}
    >
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{ remember: true }}
        onFinish={onFinish}
      >
        <Form.Item name="username" rules={[{ required: true, message: '请输入账号' }]}>
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="请输入账号"
          />
        </Form.Item>
        <Form.Item name="password" rules={[{ required: true, message: '请输入密码' }]}>
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="请输入密码"
          />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            登陆
          </Button>
          <div style={{ marginTop: 20 }}>
            或 <a href="">注册</a>
          </div>
        </Form.Item>
      </Form>
    </Card>
  );
};

export default React.memo(Login);
