// 欢迎页面
import React from 'react';
import {HeartTwoTone, SmileTwoTone} from '@ant-design/icons';
import {Alert, Card, Typography, Image} from 'antd';
import {useIntl} from 'umi';
import proChat from '../assets/images/code.png';
import  './welcome.less';

const Welcome = () => {
  const intl = useIntl();
  return (
    <div className="welcomePage">
      <Card bordered>
        <Alert
          message={intl.formatMessage({
            id: 'pro.welcome.title',
            defaultMessage: 'pro.welcome.title',
          })}
          type="success"
          showIcon
          banner
          style={{
            margin: -12,
            marginBottom: 48,
          }}
        />
        <Typography.Title level={2} style={{textAlign: 'center'}}>
          <SmileTwoTone/> Pro Chat <HeartTwoTone twoToneColor="#eb2f96"/> You
        </Typography.Title>
      </Card>
      <Card title=
              {
                intl.formatMessage({
                  id: 'pro.welcome.content',
                  defaultMessage: 'pro.welcome.content',
                })}
            style={{marginTop: 60}}
            bordered
      >
        <div className="imageBox">
          <Image
            width={200}
            src={proChat}
            style={{ borderRadius: 6 }}
          />
        </div>

      </Card>
    </div>

  )
};
export default React.memo(Welcome)
